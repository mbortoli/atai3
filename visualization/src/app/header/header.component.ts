import { Component, OnInit } from '@angular/core';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  networkColor: string;
  fileColor: string;

  networkActive = true;
  fileActive = false;

  fileList: any;

  constructor(public dataService: DataService) {
    this.fileColor = '';
    this.networkColor = 'primary';
  }

  ngOnInit() {
    this.dataService.startNetwork();
  }

  networkClicked() {
    this.fileList = undefined;
    this.networkActive = true;
    this.fileActive = false;
    this.fileColor = '';
    this.networkColor = 'primary';
  }

  fileClicked() {
    this.fileList = this.dataService.getFilesNames();
    this.networkActive = false;
    this.fileActive = true;
    this.fileColor = 'primary';
    this.networkColor = '';
  }
}
