import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-game-points',
  templateUrl: './game-points.component.html',
  styleUrls: ['./game-points.component.sass']
})
export class GamePointsComponent implements OnInit {

  @Input()
  data: any;

  view: any[] = [1920, 400];

  preparedData: any;

  constructor() { }

  ngOnInit() {
    this.preparedData = this.data.map(x => {
      console.log(x);
      return {
        name: x.gameName,
        value: x.pointsCyan
      }
    });
  }

}
