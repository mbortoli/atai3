import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {formly} from "ngx-formly-helpers";
import {RobotOptions} from "../robots";
import {EndpointService} from "../../services/endpoint.service";

@Component({
  selector: 'app-pause-task',
  templateUrl: './pause-task.component.html',
  styleUrls: ['./pause-task.component.sass']
})
export class PauseTaskComponent implements OnInit {

  form = new FormGroup({});
  model: any = {};
  fields: any[] = [
    formly.requiredSelect("robotId", "Robot ID", RobotOptions)
  ];

  constructor(private endpoint: EndpointService) { }

  ngOnInit(): void {
  }

  public submit() {
    console.log("Model is: ", this.model);
    this.endpoint.post("robot-controller/pause-task", this.model)
      .subscribe(res => console.log("Pause task send!"));
  }
}
