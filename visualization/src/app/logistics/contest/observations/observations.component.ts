import {Component, Input, OnInit} from '@angular/core';

interface IMachineName {
  rawMachineName: string;
}

interface IObservation {
  observationId: number;
  robotId: number;
  machineZone: string;
  machineName: IMachineName;
  observationTimeGame: number;
  orientation: number;
  active: boolean;
  finished: boolean;
  exploredBy: number
}

@Component({
  selector: 'app-observations',
  templateUrl: './observations.component.html',
  styleUrls: ['./observations.component.sass']
})
export class ObservationsComponent implements OnInit {

  @Input()
  data: IObservation[];


  constructor() { }

  ngOnInit(): void {
  }

}
