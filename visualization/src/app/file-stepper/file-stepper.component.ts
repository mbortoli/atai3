import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../services/data.service";
import {MatSelectChange} from "@angular/material/select";

@Component({
  selector: 'app-file-stepper',
  templateUrl: './file-stepper.component.html',
  styleUrls: ['./file-stepper.component.sass']
})
export class FileStepperComponent implements OnInit {

  @Input()
  public files: string[];

  public currentData: any[];

  public currentIndex = 0;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.currentData = [];
  }

  onSelectFile($event: MatSelectChange) {
    console.log($event.value);
    this.dataService.getFileData($event.value).subscribe((data: any[]) => {
      this.currentData = data;
      this.currentIndex = 0;
      this.dataService.updateData(this.currentData[this.currentIndex]);
      console.log(this.currentData);
    });
  }

  public back() {
    this.currentIndex--;
    this.dataService.updateData(this.currentData[this.currentIndex]);
  }

  public step() {
    this.currentIndex++;
    this.dataService.updateData(this.currentData[this.currentIndex]);
  }
}
