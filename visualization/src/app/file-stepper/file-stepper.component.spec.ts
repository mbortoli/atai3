import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileStepperComponent } from './file-stepper.component';

describe('FileStepperComponent', () => {
  let component: FileStepperComponent;
  let fixture: ComponentFixture<FileStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
