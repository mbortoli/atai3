package com.grips.dozer.converter;

import com.rcll.domain.Cap;
import com.rcll.domain.MachineClientUtils;
import org.dozer.CustomConverter;
import org.robocup_logistics.llsf_msgs.ProductColorProtos;

public class RingColorConverter implements CustomConverter {
    @Override
    public Object convert(Object dest, Object src, Class<?> destinationClass, Class<?> sourceClass) {
        if (src == null) return null;

        if (src instanceof ProductColorProtos.RingColor) {
            ProductColorProtos.RingColor protoCap = (ProductColorProtos.RingColor) src;
            switch (protoCap) {
                case RING_BLUE:
                    return MachineClientUtils.RingColor.Blue;
                case RING_GREEN:
                    return MachineClientUtils.RingColor.Green;
                case RING_ORANGE:
                    return MachineClientUtils.RingColor.Orange;
                case RING_YELLOW:
                    return MachineClientUtils.RingColor.Yellow;
            }
        } else {
            throw new RuntimeException("Error Converting ProtoTime! " + dest + "/" + src);
        }
        return dest;
    }
}
