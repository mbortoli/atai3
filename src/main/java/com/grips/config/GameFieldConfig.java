package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "gamefield")
@Getter
@Setter
public class GameFieldConfig {
    private int height;
    private int width;
    private String insertionZones;
}