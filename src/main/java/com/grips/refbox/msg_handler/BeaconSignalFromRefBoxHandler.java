package com.grips.refbox.msg_handler;

import com.grips.persistence.domain.BeaconSignalFromRefBox;
import com.grips.persistence.dao.BeaconSignalFromRefBoxDao;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class BeaconSignalFromRefBoxHandler implements Consumer<BeaconSignalProtos.BeaconSignal> {

    private final Mapper mapper;
    private final BeaconSignalFromRefBoxDao beaconSignalFromRefBoxDao;

    public BeaconSignalFromRefBoxHandler(Mapper mapper, BeaconSignalFromRefBoxDao beaconSignalFromRefBoxDao) {
        this.mapper = mapper;
        this.beaconSignalFromRefBoxDao = beaconSignalFromRefBoxDao;
    }

    @Override
    public void accept(BeaconSignalProtos.BeaconSignal beaconSignal) {
        BeaconSignalFromRefBox beacon_sig = mapper.map(beaconSignal, BeaconSignalFromRefBox.class);
        beaconSignalFromRefBoxDao.save(beacon_sig);
    }
}
