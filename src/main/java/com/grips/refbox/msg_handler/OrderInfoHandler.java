package com.grips.refbox.msg_handler;

import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.misc.GripsDataService;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.OrderInfoProtos;

import java.util.function.Consumer;
public class OrderInfoHandler implements Consumer<OrderInfoProtos.OrderInfo> {

    private final Mapper mapper;
    private final GripsDataService<ProductOrder, Long> productOrderDao;

    public OrderInfoHandler(Mapper mapper, GripsDataService<ProductOrder, Long> productOrderDao) {
        this.mapper = mapper;
        this.productOrderDao = productOrderDao;
    }

    @Override
    public void accept(OrderInfoProtos.OrderInfo orderInfo) {
        for (OrderInfoProtos.Order tmp_order : orderInfo.getOrdersList()) {
            ProductOrder productOrder = mapper.map(tmp_order, ProductOrder.class);
            //productOrder.calculateProperties();
            if (!productOrderDao.existsById(productOrder.getId())) {
                productOrder.setDelivered(false);
                productOrderDao.save(productOrder);
            }
        }
    }
}
