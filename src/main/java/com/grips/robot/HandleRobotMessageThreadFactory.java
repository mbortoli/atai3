package com.grips.robot;

import com.google.protobuf.GeneratedMessageV3;
import com.grips.config.RefboxConfig;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.rcll.protobuf_lib.RobotConnections;
import com.rcll.refbox.RefboxClient;
import com.grips.robot.msg_handler.*;
import com.grips.scheduled_tasks.SendAllKnownMachinesTask;
import com.grips.scheduler.api.DbService;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.robot_communication.services.HandleRobotMessageThread;
import com.robot_communication.services.IRobotMessageThreadFactory;
import com.robot_communication.services.RobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.Socket;
import java.util.AbstractMap;

@Service
@CommonsLog
public class HandleRobotMessageThreadFactory implements IRobotMessageThreadFactory {

    private final PrepareMachineMsgHandler prepareMachineMsgHandler;
    private final PrsTaskMsgHandler prsTaskMsgHandler;
    private final MachineOrientationMsgHandler machineOrientationMsgHandler;
    private final ReportMachineMsgHandler reportMachineMsgHandler;
    private final FoundMachinesMsgHandler foundMachinesMsgHandler;
    private final Mapper mapper;
    private final SendAllKnownMachinesTask sendAllKnownMachines;
    private final RobotConnections robotConnections;
    private final GameStateDao gameStateDao;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefboxClient refboxClient;
    private final RobotClient robotClient;
    private final DbService dbService;
    private final IScheduler productionScheduler;
    private final ExplorationScheduler explorationScheduler;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;

    private final RefboxConfig refboxConfig;

    @Value("${gameconfig.planningparameters.minorderstoplan}")
    private int minOrdersToPlan;

    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;

    public HandleRobotMessageThreadFactory(PrepareMachineMsgHandler prepareMachineMsgHandler,
                                           PrsTaskMsgHandler prsTaskMsgHandler,
                                           MachineOrientationMsgHandler machineOrientationMsgHandler,
                                           ReportMachineMsgHandler reportMachineMsgHandler,
                                           FoundMachinesMsgHandler foundMachinesMsgHandler,
                                           Mapper mapper,
                                           SendAllKnownMachinesTask sendAllKnownMachines,
                                           RobotConnections robotConnections,
                                           GameStateDao gameStateDao,
                                           BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                                           RefboxClient refboxClient,
                                           RobotClient robotClient,
                                           DbService dbService,
                                           @Qualifier("production-scheduler") IScheduler productionScheduler,
                                           ExplorationScheduler explorationScheduler,
                                           MachineInfoRefBoxDao machineInfoRefBoxDao,
                                           RefboxConfig refboxConfig) {
        this.prepareMachineMsgHandler = prepareMachineMsgHandler;
        this.prsTaskMsgHandler = prsTaskMsgHandler;
        this.machineOrientationMsgHandler = machineOrientationMsgHandler;
        this.reportMachineMsgHandler = reportMachineMsgHandler;
        this.foundMachinesMsgHandler = foundMachinesMsgHandler;
        this.mapper = mapper;
        this.sendAllKnownMachines = sendAllKnownMachines;
        this.robotConnections = robotConnections;
        this.gameStateDao = gameStateDao;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.refboxClient = refboxClient;
        this.robotClient = robotClient;
        this.dbService = dbService;
        this.productionScheduler = productionScheduler;
        this.explorationScheduler = explorationScheduler;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.refboxConfig = refboxConfig;
    }


    public HandleRobotMessageThread create(Socket _socket, AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        return new HandleRobotMessageThread(createBeaconMsgHandler(_socket), prepareMachineMsgHandler,
                prsTaskMsgHandler, machineOrientationMsgHandler, reportMachineMsgHandler, foundMachinesMsgHandler, msg);
    }

    private BeaconMsgHandler createBeaconMsgHandler(Socket socket) {
        return new BeaconMsgHandler(socket, robotConnections, gameStateDao, mapper, beaconSignalFromRobotDao,
                refboxClient, robotClient, dbService, productionScheduler, explorationScheduler,
                sendAllKnownMachines, machineInfoRefBoxDao, refboxConfig, minOrdersToPlan, numberRobots);
    }
}
