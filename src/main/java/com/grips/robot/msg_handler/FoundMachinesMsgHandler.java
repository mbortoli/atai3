package com.grips.robot.msg_handler;

import com.grips.persistence.dao.BeaconSignalFromRefBoxDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.persistence.domain.BeaconSignalFromRefBox;
import com.grips.persistence.domain.GameState;
import com.grips.persistence.domain.RobotObservation;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.grips.scheduler.exploration.MachineExplorationService;
import com.grips.tools.DiscretizeAngles;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.GripsExplorationFoundMachinesProtos;
import org.robocup_logistics.llsf_msgs.GripsMachineHandlingReportAllSeenMachinesProtos;
import org.robocup_logistics.llsf_msgs.RobotMachineReportProtos;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Consumer;

@Service
@CommonsLog
public class FoundMachinesMsgHandler implements Consumer<GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines> {

    private final Mapper mapper;
    private final GameStateDao gameStateDao;
    private final BeaconSignalFromRefBoxDao beaconSignalFromRefBoxDao;
    private final MachineExplorationService machineExplorationService;

    public FoundMachinesMsgHandler(Mapper mapper,
                                   GameStateDao gameStateDao,
                                   BeaconSignalFromRefBoxDao beaconSignalFromRefBoxDao,
                                   MachineExplorationService machineExplorationService) {
        this.mapper = mapper;
        this.gameStateDao = gameStateDao;
        this.beaconSignalFromRefBoxDao = beaconSignalFromRefBoxDao;
        this.machineExplorationService = machineExplorationService;
    }

    @Override
    public void accept(GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines foundMachines) {
        long robot_id = foundMachines.getRobotId();
        for (RobotMachineReportProtos.RobotMachineReportEntry tmp_mre : foundMachines.getMachinesList()) {
            //int rotation = (int)(tmp_mre.getRotation()*180.0/Math.PI) + 180;
            //int rotation = (int)(tmp_mre.getRotation()*180.0/Math.PI);
            int rotation = tmp_mre.getRotation();
            rotation = (rotation % 360 + 360) % 360;
            rotation = DiscretizeAngles.discretizeAngles(rotation);

            //          System.out.println("REPORT machine: " + tmp_mre.getName() + " with orientation: " + tmp_mre.getRotation() + " and side: " + tmp_mre.getSide() + " from lidar: " + tmp_mre.getFromLidar());

            RobotObservation observation = mapper.map(tmp_mre, RobotObservation.class);
            observation.setRobotId(robot_id);
            observation.setObservationTimeGame(Optional.ofNullable(
                    gameStateDao.findTop1ByOrderByGameTimeNanoSecondsDesc()).orElse(new GameState()).getGameTimeNanoSeconds());
            observation.setObservationTimeAbsolut(Optional.ofNullable(
                    beaconSignalFromRefBoxDao.findTop1ByOrderByTimeNanoSecondsDesc()).orElse(new BeaconSignalFromRefBox()).getTimeNanoSeconds());
            observation.setMachineZoneConfidence(1.0);
            observation.setOrientation(rotation);
            observation.setMachineZone(tmp_mre.getZone().toString());
            observation.setActive(false);
            observation.setFinished(false);
            observation.setExploredBy(null);
            this.machineExplorationService.updateRobotObservation(observation);
            // is saved in explorationscheduler
            //robotObservationDao.save(observation);
        }
    }
}
