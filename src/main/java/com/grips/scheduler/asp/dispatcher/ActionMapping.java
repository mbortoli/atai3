package com.grips.scheduler.asp.dispatcher;


import com.grips.config.RefboxConfig;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.persistence.misc.GripsDataService;
import com.rcll.domain.MachineClientUtils;
import com.rcll.domain.TeamColor;
import com.rcll.planning.encoding.IntOp;
import com.shared.domain.BaseColor;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommonsLog
public class ActionMapping {

    private List<String> cpConstants;
    private final GripsDataService<ProductOrder, Long> productOrderDao;

    private String colorprefix;

    public ActionMapping(GripsDataService<ProductOrder, Long> productOrderDao, RefboxConfig refboxConfig) {
        this.productOrderDao = productOrderDao;

        try {
            colorprefix = TeamColor.valueOf(refboxConfig.getTeamcolor()).equals(TeamColor.CYAN) ? "C-" : "M-";
        } catch (Exception e) {
            System.out.println("Team color undefined! Setting default to CYAN!");
            colorprefix = "C-";
        }

    }

    public void updateCodedProblem(List<String> cpConstants) {
        this.cpConstants = cpConstants;
    }

    public SubProductionTask actionToTask (IntOp action) {
        String name = action.getName();
        SubProductionTask task;
        int orderIdIndex;
        int orderId;
        int robotIndex =  action.getInstantiations()[0];
        int robotId = Character.getNumericValue(cpConstants.get(robotIndex).charAt(1));
        switch (name) {


            case "toyactiondeliveringfrombs":
                orderIdIndex =  action.getInstantiations()[1];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                Optional<ProductOrder> optorder = productOrderDao.findById((long) orderId);
                optorder.get();
                ProductOrder order = optorder.get();
                BaseColor baseColor = order.getBaseColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("toy")
                        .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.OUTPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(baseColor.toString())
                        .setOptCode(null)
                        .build();
                break;


            //ATAI parameters that have to be set for specific kind of tasks:
            //.setOrderInfoId((long) orderId) in all tasks in which you are manipulating a subpiece of the product (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide are excluded)
            //.setIsDemandTask(true) for all the tasks excluded by .setOrderInfoId (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide)
            //.setRequiredColor for retrieving a base, delivering partial product to RS to mount a ring, delivering partial product to CS to mount a cap
            //.setOptCode("RETRIEVE_CAP") for the buffer cap action
            //.setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString()) when delivering a partial product to a CS to mount a cap
            //.setOptCode(order.getDeliveryGate()+"") when delivering the final product to the DS (order is a ProductOrder object stored in the ProductOrderDAO)




            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        task.setRobotId(robotId);
        return task;
    }
}
