package com.grips.scheduler.asp;

import com.grips.config.Config;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.persistence.domain.BeaconSignalFromRobot;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.robot.RobotClientWrapper;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
@CommonsLog
public class RobotStartChecker {
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefboxClient refboxClient;
    private final SubProductionTaskDao subProductionTaskDao;
    private final RobotClientWrapper robotClientWrapper;
    public RobotStartChecker(BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                             RefboxClient refboxClient,
                             SubProductionTaskDao subProductionTaskDao,
                             RobotClientWrapper robotClientWrapper) {
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.refboxClient = refboxClient;
        this.subProductionTaskDao = subProductionTaskDao;
        this.robotClientWrapper = robotClientWrapper;
    }

    public void leaveStartingArea(int robotId) {
        SubProductionTask moveTask = SubProductionTaskBuilder.newBuilder()
                .setName("MoveToZone")
                .setState(SubProductionTask.TaskState.INWORK)
                .setType(SubProductionTask.TaskType.MOVE)
                .setOrderInfoId(null)
                .build();
        moveTask.setRobotId(robotId);
        subProductionTaskDao.save(moveTask);

        String colorprefix = refboxClient.isCyan() ? "C_" : "M_";

        if (robotId == 2)
            robotClientWrapper.sendMoveToZoneTask((long) robotId, moveTask.getId(), colorprefix + "Z52");
        else
            //robotClientWrapper.sendMoveToZoneTask(Long.valueOf(robotId), moveTask.getId(), colorprefix + "Z51");
            robotClientWrapper.sendMoveToZoneTask((long) robotId, moveTask.getId(), colorprefix + "Z52");
    }


    public boolean checkIfShouldStart(int robotId) {
        if (robotId == 1) {
            return true;
        } else {
            if (robotId == 2) {
                BeaconSignalFromRobot from1 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("1");
                if (from1 == null)
                    return false;
                return checkPose(from1.getPoseX(), from1.getPoseY());
            } else { //robotId = 3
                BeaconSignalFromRobot from2 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("2");
                if (from2 == null)
                    return false;
                return checkPose(from2.getPoseX(), from2.getPoseY());
            }
        }
    }

    private boolean checkPose(double poseX1, double poseY1) {
        if (poseY1 < 1.2)
            return false;
        else {
            double pointX = refboxClient.isCyan() ? 4.5 : -4.5;
            double pointY = 0.5;
            double distance = calculateDistance(poseX1, poseY1, pointX, pointY);
            if (distance < 1) {
                return false;
            } else {
                log.info("distance between the two robots is " + distance);
                return true;
            }
        }
    }

    private double calculateDistance(double poseX1, double poseY1, double poseX2, double poseY2) {
        return Math.sqrt(((poseX1 - poseX2) * (poseX1 - poseX2)) + ((poseY1 - poseY2) * (poseY1 - poseY2)));
    }
}
