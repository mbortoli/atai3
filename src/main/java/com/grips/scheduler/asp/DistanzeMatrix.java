package com.grips.scheduler.asp;


import com.grips.config.RefboxConfig;
import com.rcll.domain.TeamColor;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.tools.PathEstimator;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
//todo not related to GRIPS, just calculates the time needed to travel from A to B.
@Service
@CommonsLog
public class DistanzeMatrix {
    private Map<String, Double> distanceMatrix;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final PathEstimator pathEstimator;
    private final RefboxConfig refboxConfig;

    public DistanzeMatrix(MachineInfoRefBoxDao machineInfoRefBoxDao, PathEstimator pathEstimator,
                          RefboxConfig refboxConfig) {
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.pathEstimator = pathEstimator;
        this.refboxConfig = refboxConfig;
    }

    //DISTANCE MATRIX CALCULATED USING A*
    public void generateDistanceMatrix() {
        distanceMatrix = new HashMap<>();
        List<MachineInfoRefBox> machinelist = machineInfoRefBoxDao.findByTeamColor(TeamColor.valueOf(refboxConfig.getTeamcolor()));
        while (machinelist.size() < 6) {
            log.info("Machine missing...waiting!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        for (MachineInfoRefBox machine1 : machinelist) {
            String zone1 = machine1.getZone();
            for (MachineInfoRefBox machine2 : machinelist) {
                String zone2 = machine2.getZone();
                double distance;


                if (machine1.getType().equals("CS")) {
                    if (machine2.getType().equals("BS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {

                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                            distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.SHELF);
                            distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_shelf", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);

                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                            distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                        }
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);

                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("BS")) {
                    if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("RS")) {
                    if (machine2.getType().equals("BS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);

                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                            distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                            distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("DS")) {
                    if (machine2.getType().equals("BS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                    }
                }
                //starting point 115/5
                double distancefromstart;
                if (machine1.getType().equals("DS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.INPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                } else if (machine1.getType().equals("CS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.INPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.SHELF);
                    distanceMatrix.put("start" + machine1.getName() + "_shelf", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.OUTPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                } else if (machine1.getType().equals("RS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.INPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.OUTPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                } else if (machine1.getType().equals("BS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.OUTPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                }
            }
        }
    }

    public Map<String, Double> getDistanzMatrix() {
        return this.distanceMatrix;
    }
}
