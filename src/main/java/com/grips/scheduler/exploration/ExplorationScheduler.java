package com.grips.scheduler.exploration;

import com.grips.config.ExplorationConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.rcll.domain.MachineName;
import com.rcll.domain.Peer;
import com.rcll.refbox.RefboxClient;
import com.robot_communication.services.RobotClient;
import com.grips.robot.SendSuccessfullMachineReports;
import com.grips.scheduler.api.IScheduler;
import com.rcll.protobuf_lib.RobotConnections;
import com.grips.scheduler.GameField;
import com.robot_communication.services.PrsTaskCreator;
import com.shared.domain.Point2d;
import lombok.NonNull;
import lombok.Synchronized;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@CommonsLog
@Service
public class ExplorationScheduler implements IScheduler {
    private final int NUMBER_OF_MACHINES = 4; //16
    private final Map<Integer, Boolean> recentAssigned;

    private final GameField _gameField;
    private final GameStateDao gameStateDao;
    private final RefboxClient refboxClient;

    private final RobotConnections _robotConnections;

    private final ExplorationTaskDao _explorationTaskDao;
    private final PrsTaskCreator prsTaskCreator;
    private final SendSuccessfullMachineReports sendSuccessfullMachineReports;
    private final RobotClient robotClient;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final MachineExplorationService explorationService;

    private final PreProductionService preProductionService;
    private final HashMap<Long, AgentTasksProtos.AgentTask> _activeExplorationTasks = new HashMap<>();
    //todo upgrade!
    private HashMap<Peer, AgentTasksProtos.AgentTask> _previousExplorationTask = new HashMap<>();

    private boolean robot1DidRotation = false;

    private Stack<String> randomZones;

    public ExplorationScheduler(GameField gameField,
                                GameStateDao gameStateDao,
                                RobotConnections robotConnections,
                                ExplorationTaskDao explorationTaskDao,
                                PrsTaskCreator prsTaskCreator,
                                ExplorationConfig explorationConfig,
                                RefboxClient refboxClient, SendSuccessfullMachineReports sendSuccessfullMachineReports,
                                RobotClient robotClient,
                                BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                                MachineExplorationService explorationService,
                                PreProductionService preProductionService) {
        this.refboxClient = refboxClient;
        this.preProductionService = preProductionService;
        log.info("Using exploration config: " + explorationConfig.toString());
        _gameField = gameField;
        this.randomZones = new Stack<>();
        this.randomZones.push("C_Z77_exploration_0");
        this.randomZones.push("M_Z77_exploration_0");
        this.randomZones.push("C_Z12_exploration_0");
        this.recentAssigned = new ConcurrentHashMap<>();
        this.recentAssigned.put(1, false);
        this.recentAssigned.put(2, false);
        this.recentAssigned.put(3, false);

        this.gameStateDao = gameStateDao;
        _robotConnections = robotConnections;
        _explorationTaskDao = explorationTaskDao;
        this.prsTaskCreator = prsTaskCreator;
        this.sendSuccessfullMachineReports = sendSuccessfullMachineReports;
        this.robotClient = robotClient;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.explorationService = explorationService;

    }

    // if a robot is active, assign a new task, otherwise return 0
    @Synchronized
    private AgentTasksProtos.AgentTask getNextExplorationTask(@NonNull Integer robotId) {
        AgentTasksProtos.AgentTask newTask = null;
        Peer robot = _robotConnections.getRobot(robotId);
        newTask = getBestExplorationOption(robot);
        if (newTask == null) {
            newTask = getRandomExplorationTask(robotId);
        }
        return newTask;
    }

    private AgentTasksProtos.AgentTask getRandomExplorationTask(long robotId) {
        return prsTaskCreator.createExplorationTask(robotId, "XXX", randomZones.pop(), true);
    }

    @Synchronized
    public void explorationTaskResult(long robotId, @NonNull final MachineName machineName, String zoneName) {
    }

    public void explorationTaskFailed(long robotId, MachineName machineName, String zoneName) {
        log.warn("Exploration task failed[" + robotId + "] for: " + machineName + " in: " + zoneName);
        this._activeExplorationTasks.remove(robotId);
    }

    private AgentTasksProtos.AgentTask getBestExplorationOption(@NonNull Peer robot) {
        // first see, if there are observations by this robot
        List<RobotObservation> usedObservations = this.explorationService.getUsedObservations(robot.getId());

        // now we try to find the nearest observation
        // first we check, if the robot has a previous task
        Point2d prevZoneCenter = null;
        AgentTasksProtos.AgentTask prevTask = _previousExplorationTask.get(robot);
        if (prevTask != null) {
            //System.out.println("Previous task found for position estimation");
            String zoneId = prevTask.getExploreMachine().getWaypoint();
            ExplorationZone prevZone = _gameField.getZoneByName(zoneId);
            prevZoneCenter = prevZone.getZoneCenter();

            if (prevZoneCenter == null) {
                log.warn("Error that should not happen, check!!!");
                prevZoneCenter = new Point2d(0, 0);
            }
        } else {
            prevZoneCenter = new Point2d(0, 0);
        }
        RobotObservation obs = findNearestObservationNotScheduled(usedObservations, prevZoneCenter);

        AgentTasksProtos.AgentTask prsTask = null;
        if (obs != null) {
            prsTask = prsTaskCreator.createExplorationTask(robot.getId(), obs.getMachineName().getRawMachineName(), obs.getMachineZone() + "_exploration_" + obs.getOrientation(), false);
            log.info("Sending Robot " + robot.getId() + " to explore Machine " + prsTask.getExploreMachine().getMachineId()
                    + " at Zone " + prsTask.getExploreMachine().getWaypoint());
            obs.setExploredBy(robot.getId());
            obs.setActive(true);
        }
        return prsTask;
    }

    private RobotObservation findNearestObservationNotScheduled(List<RobotObservation> usedObservations, Point2d prevZoneCenter) {
        log.info("Searching nearest observation: " + usedObservations.size() + " observations found, pos is: " + prevZoneCenter.toString());
        double minDistance = Double.MAX_VALUE;
        RobotObservation nearestObservation = null;

        for (RobotObservation obs : usedObservations) {
            ExplorationZone zone = _gameField.getZoneByName(obs.getMachineZone());
            if (zone == null) {
                continue;
            }
            double distance = zone.getZoneCenter().distance(prevZoneCenter);
            boolean alreadyScheduled = isAlreadyScheduled(obs);
            log.info("Distance to " + zone.getZoneName() + " is : " + distance + " alreadyScheduled: " + alreadyScheduled);
            if (distance < minDistance && !alreadyScheduled) {
                minDistance = distance;
                nearestObservation = obs;
            }
        }

        return nearestObservation;
    }

    private boolean isAlreadyScheduled(RobotObservation obs) {
        boolean alreadyScheduled = _activeExplorationTasks.values().stream().anyMatch(t -> t.getExploreMachine().getWaypoint().equalsIgnoreCase(obs.getMachineZone()));
        //todo because of challanges  String mirroredZone = _gameField.getZoneByName(obs.getMachineZone()).getMirroredZone().getZoneName();
        // alreadyScheduled |= _activeExplorationTasks.values().stream()
        //         .anyMatch(t -> t.getExploreMachine().getWaypoint().equalsIgnoreCase(mirroredZone));
        alreadyScheduled |= _activeExplorationTasks.values().stream()
                .anyMatch(t -> new MachineName(t.getExploreMachine().getMachineId()).equals(obs.getMachineName()));
        alreadyScheduled |= _activeExplorationTasks.values().stream()
                .anyMatch(t -> new MachineName(t.getExploreMachine().getMachineId()).equals(obs.getMachineName().mirror()));

        return alreadyScheduled;
    }

    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        if (this.recentAssigned.get(robotId)) {
            this.recentAssigned.put(robotId, false);
            log.warn("Robot Recently got a task, skipping beacon signal!");
            return;
        }
        // immediately assign tasks to robot1, wait for robot2 and robot3
        if (beacon_signal.getTask().getTaskId() == -1 && checkIfShouldStart(robotId)) {
            //robot currently has no task assigned
            AgentTasksProtos.AgentTask task = null;
            task = preProductionService.getPreProductionTask(robotId);
            if (task == null) {
                task = getNextExplorationTask(robotId);
            }

            if (task != null) {
                _activeExplorationTasks.put(robot.getId(), task);
                ExplorationTask eTask = new ExplorationTask();
                eTask.setRobotId(robotId);
                eTask.setMachine(task.getExploreMachine().getMachineId());
                eTask.setZone(task.getExploreMachine().getWaypoint());
                eTask.setTimeStamp(System.currentTimeMillis());
                _explorationTaskDao.save(eTask);
                robotClient.sendPrsTaskToRobot(task);
                this.recentAssigned.put(robotId, true);
            } else {
                log.warn("No task for Robot " + robotId + " found in exploration!");
            }
        } else {
            log.debug("robot already has a task assigned, do not assign a new one");
        }

        // we also broadcast all already successfully reported machines to all robots
        sendSuccessfullMachineReports.sendSuccessfullMachineReports(robotId);
    }

    private boolean checkIfShouldStart(int robotId) {
        if (robotId == 1) {
            return true;
        } else {
            if (robotId == 2) {
                BeaconSignalFromRobot from1 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("1");
                if (from1 == null)
                    return false;
                return checkPose(from1.getPoseX(), from1.getPoseY());
            } else { //robotId = 3
                BeaconSignalFromRobot from2 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("2");
                if (from2 == null)
                    return false;
                return checkPose(from2.getPoseX(), from2.getPoseY());
            }
        }
    }

    private boolean checkPose(double poseX1, double poseY1) {
        if (poseY1 < 1.2)
            return false;
        else {
            double pointX = refboxClient.isCyan() ? 4.5 : -4.5;
            double pointY = 0.5;
            double distance = calculateDistance( poseX1, poseY1 , pointX, pointY );
            if (distance < 1) {
                return false;
            } else {
                log.info("distance between the two robots is " + distance);
                return true;
            }
        }
    }

    private double calculateDistance (double poseX1, double poseY1, double poseX2, double poseY2) {
        return Math.sqrt( ( (poseX1-poseX2) * (poseX1-poseX2) ) + ( (poseY1-poseY2) * (poseY1-poseY2) ) );
    }


    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        log.info("handeling task result: " + prsTask);
        this._activeExplorationTasks.remove((long) prsTask.getRobotId());
        return true;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        if (machine.hasZone()) {
            this.preProductionService.handleMachineInfo(machine);
        }
    }

    public void explorationTaskSucess(AgentTasksProtos.AgentTask prsTask) {
        log.info("explorationTaskResult: robotId: " + prsTask.toString());
        explorationService.markMachineAsInactive(new MachineName(prsTask.getExploreMachine().getMachineId()));
        this._activeExplorationTasks.remove((long) prsTask.getRobotId());
    }

    public boolean anyActiveTasks() {
        return preProductionService.anyTask() || !this._activeExplorationTasks.isEmpty();
    }
}
