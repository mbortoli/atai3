package com.visualization;

import com.visualization.domain.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TeamServerVisualization {
    private VisualizationGameState gameState;
    private long timeStamp;
    private List<? extends VisualizationTask> activeProductTasks;
    private List<? extends VisualizationLock> activeLockParts;
    private List<? extends VisualizationBeaconSignal> robotBeaconSignals;
    private List<? extends VisualizationExplorationZone> explorationZones;
    private List<? extends VisualizationOrder> productOrders;
    private List<? extends VisualizationRing> rings;
    private List<? extends VisualizationObservation> observations;
}
