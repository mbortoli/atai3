package com.robot_communication.services;

import lombok.NonNull;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;

public class GripsPrsTaskCreator extends PrsTaskCreator {
    public GripsPrepareMachineProtos.GripsPrepareMachine createPrepareMachineTask(@NonNull Long robotId,
                                                                                  @NonNull String machineId,
                                                                                  @NonNull String side) {
        return GripsPrepareMachineProtos.GripsPrepareMachine.newBuilder()
                .setRobotId(robotId.intValue())
                .setMachineId(machineId)
                .setMachinePrepared(true)
                .setMachinePoint(side)
                .build();
    }
}
