package com.robot_communication.services;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;

import java.util.AbstractMap;
import java.util.function.Consumer;

@CommonsLog
public class HandleRobotMessageThread extends Thread {

    private final Consumer<BeaconSignalProtos.BeaconSignal> beaconMsgHandler;
    private final Consumer<GripsPrepareMachineProtos.GripsPrepareMachine> prepareMachineMsgHandler;
    private final Consumer<AgentTasksProtos.AgentTask> prsTaskMsgHandler;
    private final Consumer<GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine> machineOrientationMsgHandler;
    private final Consumer<GripsExplorationReportMachineProtos.GripsExplorationReportMachine> explorationReportMachine;
    private final Consumer<GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines> explorationFoundMachines;


    private AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg;

    public HandleRobotMessageThread(Consumer<BeaconSignalProtos.BeaconSignal> beaconMsgHandler,
                                    Consumer<GripsPrepareMachineProtos.GripsPrepareMachine> prepareMachineMsgHandler,
                                    Consumer<AgentTasksProtos.AgentTask> prsTaskMsgHandler,
                                    Consumer<GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine> machineOrientationMsgHandler,
                                    Consumer<GripsExplorationReportMachineProtos.GripsExplorationReportMachine> explorationReportMachine,
                                    Consumer<GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines> explorationFoundMachines,
                                    AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        this.beaconMsgHandler = beaconMsgHandler;
        this.prepareMachineMsgHandler = prepareMachineMsgHandler;
        this.prsTaskMsgHandler = prsTaskMsgHandler;
        this.machineOrientationMsgHandler = machineOrientationMsgHandler;
        this.explorationReportMachine = explorationReportMachine;
        this.explorationFoundMachines = explorationFoundMachines;
        this.msg = msg;
    }

    @Override
    public void run() {
        handleMsg(this.msg);
    }

    private void handleMsg(AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        try {
            if (msg.getKey() instanceof BeaconSignalProtos.BeaconSignal) {
                beaconMsgHandler.accept(BeaconSignalProtos.BeaconSignal.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsPrepareMachineProtos.GripsPrepareMachine) {
                prepareMachineMsgHandler.accept(GripsPrepareMachineProtos.GripsPrepareMachine.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsMidlevelTasksProtos.GripsMidlevelTasks) {
                prsTaskMsgHandler.accept(AgentTasksProtos.AgentTask.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine) {
                machineOrientationMsgHandler.accept(GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsExplorationReportMachineProtos.GripsExplorationReportMachine) {
                explorationReportMachine.accept(GripsExplorationReportMachineProtos.GripsExplorationReportMachine.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines) {
                explorationFoundMachines.accept(GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines.parseFrom(msg.getValue()));
            } else if(msg.getKey() instanceof AgentTasksProtos.AgentTask) {
                prsTaskMsgHandler.accept(AgentTasksProtos.AgentTask .parseFrom(msg.getValue()));
            } else {
                log.error("Unknown message in RobotHandler! " + msg.getKey().getClass().getSimpleName());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("Can't parse msg in RobotHandler!", e);
        }
    }

}
