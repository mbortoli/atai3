package com.shared.domain;

public enum GameState {
    INIT,
    WAIT_START,
    RUNNING,
    PAUSED
}
