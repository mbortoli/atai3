package com.shared.domain;

public enum MachineSide {
    INPUT,
    OUTPUT,
    SHELF,
    SLIDE
}
